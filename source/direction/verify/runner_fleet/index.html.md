---
layout: markdown_page
title: "Category Direction - Runner Fleet"
description: " Features and capabilities for installing, configuring, managing and monitoring a fleet of GitLab Runners."
canonical_path: "/direction/verify/runner_fleet/"
---

- TOC
  {:toc}

## Runner Fleet

Our vision is to provide users with a birds-eye view, configuration management capabilities, and predictive analytics to easily administer a fleet of GitLab Runners.  This category's north star delivers an industry-leading user experience as measured by system usability scores and the lowest maintenance overhead for customers who self-manage a CI build platform at scale. Features in this category will primarily span the Admin and Group views in the GitLab UI.

## Who we are focusing on?

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Runner, our "What's Next & Why" are targeting the following personas, as ranked by priority for support:

1. [Priyanka - Platform Engineer](/handbook/product/personas/#priyanka-platform-engineer)
2. [Devon - DevOps Engineer](/handbook/product/personas/#devon-devops-engineer)
3. [Sasha - Software Developer](/handbook/product/personas/#sasha-software-developer)
4. [Delaney - Development Team Lead](/handbook/product/personas/#delaney-development-team-lead)

## Strategic Priorities

The table below represents the current strategic priorities for Runner Fleet. This list will change with each monthly revision of this direction page.

|Item|Why?| Target delivery QTR |
|----------|----------------|---------------------|
|[Runner Bulk Delete](https://gitlab.com/groups/gitlab-org/-/epics/8144)|The 2021 Q3 UX scorecard of the current runner administrative user experience identified several areas that needed improvement regarding workflow and user experience. Throughout FY23 Q1 FY2023, we have iteratively redesigned and added new features to the Admin Area - Runners view and in 14.10 the Group view was also updated. The subsequent iteration will add a bulk deletefeature to the [Admin Area > Runners view](https://gitlab.com/gitlab-org/gitlab/-/issues/339525) and the [Group view](https://gitlab.com/gitlab-org/gitlab/-/issues/361721) for managing runners.| Q2 FY 2023              |
|[Runner Version Management](https://gitlab.com/groups/gitlab-org/-/epics/8145)|The ability to view, search, and filter by runners running older versions will simplify maintenance and operations for GitLab namespace administrators. The initial iteration of this feature - [the upgrade status badge in the admin view](https://gitlab.com/gitlab-org/gitlab/-/issues/22224), shipped in 15.1. Next, we are adding the summary count of outdated runners metric in the admin view. In future iterations, we will add the upgrade status badge and count of outdated runners metric to the [group view](https://gitlab.com/gitlab-org/gitlab/-/issues/363614).|Q2 & Q3 FY 2023|
|[Runner Search, Association and Ownership for Admin and Group views](https://gitlab.com/groups/gitlab-org/-/epics/7181)|In customer interviews, a recurring theme and pain point for administrators of large GitLab installations is the difficulty of determining runner ownership for non-instance (Shared Runners.) This problem is exacerbated in scenarios where GitLab administrators are asked to troubleshoot time-critical production issues with CI jobs but cannot determine vital information regarding the Runner. This feature set aims to solve this pain point and save administrators countless hours researching runner provenance each month.To start, including an assigned group or project data element and adding the ability to export the runner data from GitLab will significantly simplify the operational overhead of locating and troubleshooting runners across an enterprise installation of GitLab.| Q3 FY 2023             ||
|[Runner Fleet - Runner queue wait times](https://gitlab.com/groups/gitlab-org/-/epics/5667)| A critical question for our customers who use GitLab SaaS or those managing GitLab at scale is the estimated wait time for a job to start on a Runner. This problem and the related use cases are the ones that we plan to address as we evolve the Runner Fleet [vision](https://gitlab.com/gitlab-org/gitlab/-/issues/345594/) and implement new features associated with Runner queue monitoring and fleet performance. The initial [MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/335102) for this feature will display the average wait time for CI job queues for instance-level (Shared Runners) in the Admin Area > Runners view.|Q4 FY 2023|   

## Maturity Plan

- Runner Fleet is at the ["Viable"](/direction/maturity/) maturity level.
- As detailed in this [issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1995), we are working towards establishing the baseline maturity score for Runner Fleet.

## Competitive Landscape

At GitLab, a critical challenge that we are trying to solve is simplifying the administration and management of a CI/CD build fleet at an enterprise scale. We notice that other competitors are also investing in this general category. Github recently [announced](https://github.blog/2022-02-23-new-way-understand-github-hosted-runner-capacity/) a new management experience that provides a summary view of GitHub-hosted runners. This is a signal that there will be a focus on reducing maintenance and configuration overhead for managing a CI/CD build environment at scale across the industry.

GitHub recently opened a new issue on their public roadmap, [actions seamless management for GitHub-hosted & self-hosted runners](https://github.com/github/roadmap/issues/504). The goal is to launch a "seamless management interface for GitHub runners." So clearly, the market is validating the Runner Fleet vision driven by our customer feedback of managing a DevOps platform at an enterprise scale.

With this [new feature](https://github.com/github/roadmap/issues/503) on GitHub's roadmap to enable management of runners without administrator privileges, it is clear that there is recognition in the market that simplifying the administrative maintenance and overhead of the CI build fleet is critical for enterprise customers.

## Give Feedback

The near features highlighted here represent just a subset of the features and capabilities that have been requested by the community and customers. If you have questions about a specific runner feature request or have a requirement that's not yet in our backlog, you can provide feedback or open an issue in the GitLab Runner [repository](https://gitlab.com/gitlab-org/gitlab-runner/-/issues).

## Revision Date

This direction page was revised on: 2022-06-23